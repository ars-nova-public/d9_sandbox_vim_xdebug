# Debugging Drupal 9 in vim using xdebug

${toc}

## Goals

1. Questions & caveats
1. Set up environment for xdebug
1. Test via browser
1. Test via cli

## Target audience

Questions & caveats

- Is this productive?
- Do I want to use it?

Discuss where this video lies wrt:
- vim
- docker env
- Drupal and drush

## Xdebug components

- Web server (Apache / PHP) with xdebug
- Browser or CLI
- IDE

## My environment

Bare bones

- Server: docker /
  [docker4drupal](https://github.com/wodby/docker4drupal) / 
  [docker4php](https://github.com/wodby/docker4php)
- Browser: Brave with
  [Xdebug helper extension](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?hl=en)
- IDE [sic]: vim with
   - [vim-plug](https://github.com/junegunn/vim-plug) 
   - [vdebug plug-in](https://github.com/vim-vdebug/vdebug)

## Setup 

### Browser Brave

Brave with
 [Xdebug helper extension](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?hl=en)

### Docker

1. Review docker-composer.yml

### Editor / IDE

#### vim

- [vimplug](https://github.com/junegunn/vim-plug)
- [vdebug plugin](https://github.com/vim-vdebug/vdebug)
- .vimrc
   - install vdebug
- project/.vimrc

## Debug

### vim

1. Open file in vim
1. Set breakpoint
1. Start vdebug

### Browser

1. Turn on 'debug' in Xdebug helper
1. Reload page

### cli

```
export XDEBUG_SESSION=1;

# run file via php
php ./web/index.php

# drush via php
php ./vendor/drush/drush/drush.php status


