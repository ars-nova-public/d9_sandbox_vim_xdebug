syntax on
:scriptencoding utf-8
set nocompatible
" for loading addl .vimrc from project root directory
set exrc

" ---------------------------------------------------------------------------
" plugins
" ---------------------------------------------------------------------------

call plug#begin('~/.vim/plugged')

" themes
Plug 'morhetz/gruvbox'
" debug
Plug 'vim-vdebug/vdebug'

" nerdtree
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

" markdown
Plug 'plasticboy/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }

call plug#end()
" ---------------------------------------------------------------------------


" ---------------------------------------------------------------------------
" appearance
" ---------------------------------------------------------------------------

let g:gruvbox_italic=1
let g:gruvbox_italicize_comments=1
set background=dark
colo gruvbox

" transparent bg
highlight Normal     ctermbg=NONE guibg=NONE
highlight LineNr     ctermbg=NONE guibg=NONE
highlight SignColumn ctermbg=NONE guibg=NONE

" ---------------------------------------------------------------------------
" xdebug
" ---------------------------------------------------------------------------

set updatetime=500

hi default DbgBreakptLine term=reverse ctermfg=NONE ctermbg=NONE guifg=#00ff00 guibg=#009900
let g:vdebug_options = {
\  'port' : 9003,
\  'break_on_open' : 0,
\  'simplified_status' : 0,
\  'debug_window_level' : 0,
\  'debug_file_level' : 0,
\  'debug_file' : '~/vdebug.log',
\  'watch_window_style' : 'compact',
\  'path_maps' : {
\  }
\}
" look in project .vimrc for paths


let g:vdebug_keymap = {
\  "run" : "<F5>",
\  "run_to_cursor" : "<S-F5>",
\  "step_over" : "<F6>",
\  "step_into" : "<F7>",
\  "step_out" : "<F8>",
\  "close" : "<F10>",
\  "detach" : "<F11>",
\  "set_breakpoint" : "<F9>",
\  "get_context" : "<S-F11>",
\  "eval_under_cursor" : "<S-F12>",
\  "eval_visual" : "<Leader>e",
\}
let g:vdebug_features = {
\   'max_depth' : 3,
\   'max_children' : 20,
\   'max_data' : 1024,
\}

" orig
"let g:vdebug_keymap = {
"   \    "run" : "<F5>",
"   \    "run_to_cursor" : "<F9>",
"   \    "step_over" : "<F2>",
"   \    "step_into" : "<F3>",
"   \    "step_out" : "<F4>",
"   \    "close" : "<F6>",
"   \    "detach" : "<F7>",
"   \    "set_breakpoint" : "<F10>",
"   \    "get_context" : "<F11>",
"   \    "eval_under_cursor" : "<F12>",
"   \    "eval_visual" : "<Leader>e",
"   \}

