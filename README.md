# d9_sandbox_vim_xdebug

${toc}

## Description

This repository provides the elements necessary to configure vim for debugging
a Drupal 9 site from the browser and the command line.

## Contains

- docker compose file
- Drupal 9 site skeleton:
  - composer.json
  - Initialization database
- .vimrc file

## Requirements

In order to use this repository to test vim debugging, you must have the
following installed:

- docker
- docker-compose
- vim (latest version)
- [vim-plug](https://github.com/junegunn/vim-plug) (plugin manager)
- Chromium-based browser with
  [Xdebug helper extension](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?hl=en)
  or equivalent

## Additional Reference

Videos

- [Debugging Drupal (and drush) in vim](https://odysee.com/@ArsNova:6/drupal-debug-vim:6)
- [Tweaking vim debug: Extending apache timeout, clearing Drupal cache from vim](https://odysee.com/@ArsNova:6/vim-xdebug-apache-timeout:8)

## Installation

Before beginning, ensure that you have vim-plug installed.

**Warning**: the following will overwrite your existing .vimrc file.
I recommend that you make a backup prior to proceeding. (You do make frequent
backups of your home directory and keep on your important config files in a
git repository, right?)

1. Clone repository

```
git clone git@gitlab.com:ars-nova-public/d9_sandbox_vim_xdebug.git
cd d9_sandbox_vim_xdebug
```

2. Configure vim

   _(When running vim commands below preceded by a colon, be sure to type them
   out, rather than copy/pasting them.)_

```
# copy the provided .vimrc to your home directory
cp ./addl/.vimrc ~/

# start up vim
vim

# once inside vim, run
:PlugInstall
:q

# edit the project .vimrc, changing the right side of the path_maps to reflect
# the project path on your workstation.
:e ./.vimrc

# exit vim
:q
```

3. Add the following hostname to your /etc/hosts file

```
127.0.0.1          d9.localhost
```

## Startup

1. Start up the containers and install Drupal

```
# start the containers
make up

# create the files directory
mkdir drupal/web/sites/default/files
chmod 777 drupal/web/sites/default/files

# enter the php container shell
make shell

# install D9
cd drupal
composer install
drush cr

# exit the container
exit
```

2. Begin debugging

- Vist http://d9.localhost:8000
- Follow instructions in the video
